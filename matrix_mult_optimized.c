#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define M 1<<11

/*
 *1st Optimization. 
 *Convert the matrix from 2D to 1D. Also, the allocation using malloc dynamically, and transpose the 2nd matrix, so that, second matrix column beccomes a row, and will fit in a 
 *cache line.
 */


//int A[N][N], B[N][N], C[N][N] ;
int *A, *B, *C, *T;
int tile_size = 2048;

/******************************************************************************/

double cpu_time ( void )

/******************************************************************************/
/*
 *   Purpose:
 *
 *   	CPU_TIME returns the current reading on the CPU clock.
 *
 *   	  Discussion:
 *
 *     	The CPU time measurements available through this routine are often
 *   	not very accurate.  In some cases, the accuracy is no better than
 *   	a hundredth of a second.  
 *
 *   	 Licensing:
 *
 *   	This code is distributed under the GNU LGPL license.
 *
 *   	Modified:
 *
 *   	06 June 2005
 *
 *   	Author:
 *
 *      John Burkardt
 *
 *   	Parameters:
 *
 *      Output, double CPU_TIME, the current reading of the CPU clock, in seconds.
 *   			  	  	  	  	*/
{
  double value;

  value = ( double ) clock ( )
    	/ ( double ) CLOCKS_PER_SEC;

  return value;
}

/*
 * Original code develpoed by Lavnya Ramapantulu
 * Modified by Arkaprava Basu
 */

main()
{
    int i, j, k, prod_ij;
    double time[3], start, stop;
	
	int temp1 = 0, temp2 = 0, N = M;
	A = (int *)malloc(sizeof(int) * N * N);
	B = (int *)malloc(sizeof(int) * N * N);
	C = (int *)malloc(sizeof(int) * N * N);
	
    /* Initialize the matrices */
    
    start = cpu_time();

    temp1 = 0;
    for(i=0; i<N; ++i){
   	 for(j=0; j<N; ++j){
   		 A[temp1 + j]=i-j;
   		 B[temp1 + j]=i+j;
		 C[temp1 + j]=0;
   	 }
	temp1 += N;
    }
    stop = cpu_time();

    time[0] = stop - start;

    /* Naive Matrix Multiplication.
 *    	Improve the performance of this code
 *    	    	*/

    /* copy the matrix into new one now. */
    T = (int *)malloc(sizeof(int) * N * N);
    temp1 = 0;
    temp2 = 0;
    for(i = 0; i < N; i++){
	temp2 = 0;
	for(j = 0; j < N; j++){
		T[temp2 + i] = B[temp1 + j];
		temp2 += N;
	}
	temp1 += N;
    }

    start = cpu_time();
    
    /* First Optimizattion */
    /* 
    temp1 = 0;
    temp2 = 0;
    for(i=0; i<N; ++i){
	temp2 = 0;
   	 for(j=0; j<N; ++j){
   		 for(k=0; k<N; ++k){
   			 C[temp1 + j] += A[temp1 + k] * T[temp2 + k];
   		 }
		temp2 += N;
   	 }
	temp1 += N;
    }
    */


    /* second Optimization, built on top of 1st optimization */
    int i1 = 0, j1 = 0;
    for(i1 = 0; i1 < N; i1 += tile_size){
    	for(j1 = 0; j1 < N; j1 += tile_size){
		int ir1 = 0, il1 = i1;
		for(ir1 = 0; ir1 < N; ir1 += tile_size){
			for(il1 = i1; il1 < i1 + tile_size; il1++){
				for(i = ir1; i < ir1 + tile_size; i++){
					for(j = j1; j < j1 + tile_size; j++){
						C[il1 * N + i] += A[il1 * N + j] * T[i * N + j];
					}
				}
			}
		}
	}
    }
    stop = cpu_time();

    time[1] = stop - start;

    start = cpu_time();

    /* You should improve the matrix multiplication Code below*/
/*    for(i=0; i<N; ++i){
   	 for(j=0; j<N; ++j){
   		 for(k=0; k<N; ++k){
   		     C[i][j]  += A[i][k]*B[k][j];
   		 }
   	 }
    }
*/

exit:

    stop = cpu_time();

    time[2] = stop - start;

    //printf("Matrix Initialization Time: %9f\n", time[0]);
    printf("Your Matrix Multiplication Time: %9f\n", time[1]);
    //printf("Naive Matrix Multiplication Time: %9f\n", time[2]);
    //printf("Your code speedup when compared with Naive method: %1.2fx\n", time[2]/time[1]);
}

